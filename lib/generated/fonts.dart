class Fonts {
  Fonts._();

  static const String fontFamilyTitle = 'title';
  static const String fontFamilyComponentText = 'component_text';
  static const String fontFamilyMostFocus = 'most_focus';
}
